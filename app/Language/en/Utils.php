<?php

return [
    'add' => "Add",
    'refresh' => "Refresh",
    'save' => "Save",
    'cancel' => "Cancel",
    'saving' => "Saving",
    'error_ajax' => "There was an error, please contact administrator.",
    'yes' => "Yes",
    'no' => "No",
    'update' => "Update",
];