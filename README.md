# myth-auth-management

Using the reposotiry from https://github.com/lonnieezell/myth-auth to apply the management section:

- Codeigniter 4.1.5
- Template on Bootstrap 5
- Swal
- Toastr
- Language for the main points
- CRUD for Roles
- CRUD for Permissions
- Assign actions to Users and Assign Roles to User
